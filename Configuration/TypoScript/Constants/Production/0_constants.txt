#################################
## NAVIGATION MOBILE CONSTANTS ##
#################################

plugin.tx_hive_cpt_nav_mobile {
	settings {
        lib {
            navigation {
                special = 2
                cache {
                    postfix = default
                }
            }
        }
        production {
            includePath {
                public = EXT:hive_cpt_nav_mobile/Resources/Public/
                private = EXT:hive_cpt_nav_mobile/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_cpt_nav_mobile/Resources/Public/
                }

            }
            optional {
                active = 1
            }
        }
    }
}