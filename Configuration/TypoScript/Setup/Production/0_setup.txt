#############################
## NAVIGATION MOBILE SETUP ##
#############################


[globalVar = LIT:1 > {$plugin.tx_hive_cfg_typoscript.settings.gulp}]
    #not in use (BS4)
    #page {
    #    includeCSS {
    #        hive_cpt_nav_mobile__css = {$plugin.tx_hive_cpt_nav_mobile.settings.production.includePath.private}Assets/Less/includeCSS/index.less
    #        hive_cpt_nav_mobile__css.media = screen
    #    }
    #    includeJSFooterlibs {
    #        hive_cpt_nav_mobile__js = {$plugin.tx_hive_cpt_nav_mobile.settings.production.includePath.public}Assets/Js/includeJSFooterlibs/use_mobileNavigation.min.js
    #        hive_cpt_nav_mobile__js.async = 1
    #    }
    #}
[global]


lib.navigation.button.mobile = TEXT
lib.navigation.button.mobile.value (
    <button id="buttonHamburger" type="button" class="buttonHamburger">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar last"></span>
        <script type="text/javascript">var tx_hive_cpt_nav_mobile__buttonHamburger = true;</script>
    </button>
)

lib.navigation.mobile = COA
lib.navigation.mobile {

	5 = LOAD_REGISTER
    5  {
		pageUid.cObject = TEXT
        pageUid.cObject {
            field = pageUid
            ifEmpty = {$plugin.tx_hive_cpt_nav_mobile.settings.lib.navigation.special}
        }
		cachePostfix.cObject = TEXT
        cachePostfix.cObject {
            field = cachePostfix
            ifEmpty = {$plugin.tx_hive_cpt_nav_mobile.settings.lib.navigation.cache.postfix}
        }
    }

	10 = HMENU
	10 {
	    special = directory
        special.value.data = register:pageUid
		1 = TMENU
		1 {
			wrap = <nav class="hamburger" role="nav"><div class="dropdown"><button id="hamburger" type="button" class="dropdown-toggle menu hamburger" data-toggle="dropdown"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar last"></span></button><ul class="dropdown-menu" aria-labelledby="hamburger">|</ul></div><script type="text/javascript">var tx_hive_cpt_nav_mobile__nav = true;</script></nav>
			expAll = 1
			noBlur = 1
			NO = 1
			NO {
				ATagTitle.field = abstract // description // title
				ATagBeforeWrap = 1
				linkWrap = |
				wrapItemAndSub = <li class="">|</li>
				wrapItemAndSub.override.cObject = COA
				wrapItemAndSub.override.cObject {
					if {
						value = 4
						equals.field = doktype
						isTrue = 1
						isTrue.if {
							value.data = TSFE:page|uid
							equals.field = shortcut
						}
					}

					10 = TEXT
					10.value = <li class="active dropdown">|</li>
				}
			}

			ACT < .NO
			ACT {
				wrapItemAndSub = <li class="active">|</li>
			}

			CUR < .ACT
			CUR {
                wrapItemAndSub = <li class="active current">|</li>
            }
			IFSUB < .NO
			IFSUB {
				doNotLinkIt = 0
				allWrap = |<span data-toggle="dropdown" class="lvl2-toggle glyphicon glyphicon-chevron-down"></span>
				wrapItemAndSub = <li class="dropdown">|</li>
			}

			ACTIFSUB < .IFSUB
			ACTIFSUB {
				wrapItemAndSub = <li class="active dropdown">|</li>
			}

			CURIFSUB < .ACTIFSUB
			CURIFSUB {
                wrapItemAndSub = <li class="active current dropdown">|</li>
            }
		}
		2 < .1
		2 {
			wrap = <ul class="dropdown-menu v2">|</ul>
			SPC = 1
			SPC {
				wrapItemAndSub = <li class="divider"></li><li class="dropdown-header">|</li>
			}
		}
		3 < .2
        4 < .3
        5 < .4
        5 {
            IFSUB >
            ACTIFSUB >
            CURIFSUB >
        }
	}
}

# Caching for Menu
[globalVar = LIT:1 = {$plugin.tx_hive_cpt_nav_mobile.settings.production.optional.active}]
lib.navigation.mobile {
    stdWrap {
        cache {
            /**
             * Hier wird die aktuelle Sprache benutzt, um einen Inhalt pro Sprache
             * für alle Seiten zu cachen.
             */
            key = tx_hive_cpt_nav_mobile_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            key.insertData = 1

            // Mit den selbstgewählten Tags kann der Cache später gezielt geleert werden.
            tags = tx_hive_cpt_nav_mobile_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            tags.insertData = 1

            /**
             * Wenn eine neue Seite generiert wird, ist das Kontaktmenü max. eine Stunde alt.
             * Wenn die Seite alle 24 Stunden neu generiert wird, könnte das Kontaktmenü
             * also auch über einen Tag alt sein!
             */
            # 3600 x 24 x 30
            lifetime = 2592000
        }
    }

    4 = TEXT
    4 {
        data = date: dmyHis
        wrap = <!--CACHED_|-->
    }
}
[global]